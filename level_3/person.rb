# Here should be the `Person` class

# Here should be the `Person` class
class Person
  attr_accessor :gender
  attr_reader :id,:first_name, :last_name, :email

  def initialize(id ,gender, first_name, last_name, email)
    @id = id
    @gender = gender
    @first_name = first_name
    @last_name = last_name
    @email = email
  end

  def male?
    true if @gender == 'M'
  end

  def female?
    true if @gender == 'F'
  end
end
