require_relative "person"
source_path = File.join(__dir__, "source.csv")

fichier_source = File.open("source.csv", "r")

# it's up to you now!
def parse(input)
  h = Hash.new(0)

  input.each_with_index { |line, index|
    h[index] = line.split(',')
  }

  h.each { |key, value|
    id = key
    gender = value[0]
    first_name = value[1]
    last_name = value[2]
    email = value[3].gsub("\n", '')

    h[key] = Person.new(id, gender , first_name, last_name, email)

  }
end

h = parse(fichier_source)

