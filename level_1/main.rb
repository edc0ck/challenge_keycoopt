fichier_source = File.open("source.csv", "r")

def parse(input)
  h = Hash.new(0)
  input.each_with_index { |line, index |
    h[index] = line.split(',')
  }
   h
end
p parse(fichier_source)