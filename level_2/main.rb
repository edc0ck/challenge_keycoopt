require_relative "person"
# it's up to you now!
fichier_source = File.open("source.csv", "r")

def parse(input)
  h = Hash.new(0)

  input.each_with_index { |line, index|
    h[index] = line.split(',')
  }

  h.each { |key, value|
    title = value[0]
    first_name = value[1]
    last_name = value[2]
    email = value[3].gsub("\n", '')

    h[key] = Person.new(title, first_name, last_name, email)
  }
end